# branching_out

Practicing things I don't get to use at work.

## Things

### Frontend
- [] HTML
- [] Javascript libraries - Sencha Ext JS, React, AngularJS, or jQuery
- [] CSS
### Middleware
- [] Go
- [] .NET
- [] C#
### Backend
- [] Postgres or MySQL
### Web
- [] GraphQL
### Deployment
- [] CI/CD (GitlabCI)
- [] AWS / Azure / GCP
- [] Rancher & Kubernetes
